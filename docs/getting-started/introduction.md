---
layout: doc
---

# introduction

## what is nano/modules/testing?

`nano/modules/testing` is a collection of useful testing tools for nano

testing module includes following packages:

- [pest](https://pestphp.com/)
- [codeception](https://codeception.com/)
- [phpstan](https://phpstan.org/)
- [php-cs-fixer](https://github.com/PHP-CS-Fixer/PHP-CS-Fixer)
- [phinx](https://phinx.org/)
- [mockery](https://docs.mockery.io/en/latest/)





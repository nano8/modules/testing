---
layout: home

hero:
    name:    nano/modules/testing
    tagline: essential testing tools for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/testing is a collection of useful testing tools for nano
    -   title:   unit, integration and e2e testing
        details: |
                 nano/modules/testing provides a php/pest and codeception
    -   title:   code quality tools
        details: |
                 phpstan and php-cs-fixer included
---

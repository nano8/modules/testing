<div align="center">

### nano/modules/testing

---

[![release](https://img.shields.io/packagist/v/laylatichy/nano-modules-testing?style=for-the-badge&logo=packagist&label=laylatichy/nano-modules-testing)](https://packagist.org/packages/laylatichy/nano-modules-testing)

![php](https://img.shields.io/packagist/dependency-v/symfony/symfony/php?style=for-the-badge&logo=php)

![PHPStan](https://img.shields.io/badge/PHPStan-Level%20max-brightgreen.svg?style=for-the-badge&logo=phpstan)
![Total Downloads](https://img.shields.io/packagist/dt/laylatichy/nano-modules-testing?style=for-the-badge)

![dev](https://img.shields.io/gitlab/pipeline-status/nano8%2Fmodules%2Ftesting?gitlab_url=https%3A%2F%2Fgitlab.com&branch=dev&style=for-the-badge&logo=gitlab&label=dev)
![master](https://img.shields.io/gitlab/pipeline-status/nano8%2Fmodules%2Ftesting?gitlab_url=https%3A%2F%2Fgitlab.com&branch=master&style=for-the-badge&logo=gitlab&label=master)
![release](https://img.shields.io/gitlab/pipeline-status/nano8%2Fmodules%2Ftesting?gitlab_url=https%3A%2F%2Fgitlab.com&branch=release&style=for-the-badge&logo=gitlab&label=release)

[![docs](https://img.shields.io/badge/Reference-2b2e3b?style=for-the-badge&logo=data:image/svg%2bxml;base64,PHN2ZyB3aWR0aD0iODAwcHgiIGhlaWdodD0iODAwcHgiIHZpZXdCb3g9IjAgMCAyNCAyNCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4NCjxwYXRoIGQ9Im05IDExYy0wLjU1MjI4IDAtMSAwLjQ0NzctMSAxczAuNDQ3NzIgMSAxIDFoNmMwLjU1MjMgMCAxLTAuNDQ3NyAxLTFzLTAuNDQ3Ny0xLTEtMWgtNnptMCAzYy0wLjU1MjI4IDAtMSAwLjQ0NzctMSAxczAuNDQ3NzIgMSAxIDFoNmMwLjU1MjMgMCAxLTAuNDQ3NyAxLTFzLTAuNDQ3Ny0xLTEtMWgtNnptMy40ODItMTJjMC42Nzg4LTAuMDAxMDQgMS4yODEyLTAuMDAxOTYgMS44Mzc2IDAuMjI4NTFzMC45ODE4IDAuNjU3MTEgMS40NjEgMS4xMzc4YzAuOTQ5NiAwLjk1MjUxIDEuOTAwNyAxLjkwMzYgMi44NTMyIDIuODUzMiAwLjQ4MDcgMC40NzkyNSAwLjkwNzQgMC45MDQ1OSAxLjEzNzggMS40NjEgMC4yMzA1IDAuNTU2NDEgMC4yMjk2IDEuMTU4OCAwLjIyODUgMS44Mzc3LTAuMDAzOCAyLjUxNTktMWUtNCA1LjAzMTgtMWUtNCA3LjU0NzggMWUtNCAwLjg4NjUgMWUtNCAxLjY1MDMtMC4wODIxIDIuMjYxOS0wLjA4ODIgMC42NTU1LTAuMjg2OSAxLjI4MzktMC43OTY2IDEuNzkzNi0wLjUwOTYgMC41MDk2LTEuMTM4IDAuNzA4NC0xLjc5MzUgMC43OTY1LTAuNjExNyAwLjA4MjItMS4zNzU1IDAuMDgyMi0yLjI2MiAwLjA4MjFoLTYuMTMxNmMtMC44ODY1IDFlLTQgLTEuNjUwMyAxZS00IC0yLjI2Mi0wLjA4MjEtMC42NTU1MS0wLjA4ODEtMS4yODM5LTAuMjg2OS0xLjc5MzUtMC43OTY1LTAuNTA5NjYtMC41MDk3LTAuNzA4NC0xLjEzODEtMC43OTY1My0xLjc5MzYtMC4wODIyNC0wLjYxMTYtMC4wODIyLTEuMzc1NC0wLjA4MjE1LTIuMjYxOWwxZS01IC0xMC4wNjZjMC0wLjAyMjAyLTFlLTUgLTAuMDQzOTctMWUtNSAtMC4wNjU4My01ZS01IC0wLjg4NjQ5LTllLTUgLTEuNjUwMyAwLjA4MjE1LTIuMjYyIDAuMDg4MTMtMC42NTU1MSAwLjI4Njg3LTEuMjgzOSAwLjc5NjU0LTEuNzkzNSAwLjUwOTY2LTAuNTA5NjcgMS4xMzgtMC43MDg0MSAxLjc5MzUtMC43OTY1NCAwLjYxMTY2LTAuMDgyMjQgMS4zNzU1LTAuMDgyMiAyLjI2Mi0wLjA4MjE1IDEuMTgyNiA3ZS01IDIuMzY1MiAwLjAwMTY4IDMuNTQ3OC0xLjRlLTR6IiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiNmMWZhOGMiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPg0KPC9zdmc+DQo=&label=documentation&labelColor=3b3e4b)](https://nano8.gitlab.io/modules/testing/)

##### project supported by

[![jetbrains](https://resources.jetbrains.com/storage/products/company/brand/logos/jb_beam.svg)](https://jb.gg/OpenSourceSupport)

</div>